<?php
function ubah_huruf($string){
    $arrRes = [];
    $arrString = str_split($string);
    $length = strlen($string);
    for($i = 0; $i < $length; $i++) {
        array_push($arrRes, ++$arrString[$i]);
        echo $arrRes[$i];
    }
    echo "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
